void function2();

void function()
{
	{
		f1();
	}

	if(true)
		f2();

	if(false)
	{
		f3();
	}

	true?f4():f5();

	if(true)
	{
		f6();
		if(1)
			f7();
		else
			f8();
	}
	else if(false)
		f9();
	else
	{
		f10();
	}

x:	for(;;)
		f11();

	while(true)
		if(1)
			f12();

	do
	{

	}
	while(1);

	goto x;
}

void function2()
{
	f1();
	// function();
	sizeof(int);
}