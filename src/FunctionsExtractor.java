import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.io.FilenameUtils;

import model.ExecutionPath;
import model.Function;
import model.FunctionCall;
import model.LogEntry;
import model.LogEntry.LogEntryType;
import nu.xom.Builder;
import nu.xom.Document;
import nu.xom.Element;
import nu.xom.Nodes;
import nu.xom.ParsingException;
import nu.xom.ValidityException;
import utilities.XPath;

public class FunctionsExtractor {
	private static final String NEW_LINE = System.getProperty("line.separator");

	private static String SRC_ML = "srcml";
	private static String crticalSectionStartMatcher;
	private static String crticalSectionEndMatcher;

	private static List<LogEntry> logEntries = new ArrayList<LogEntry>();

	public static void main(String[] args) {
		PrintStream sysoutBackUP = System.out;
		PrintStream syserrBackUP = System.err;
		try {
			System.setOut(new PrintStream(new FileOutputStream(new File("log.txt"))));
			System.setErr(new PrintStream(new FileOutputStream(new File("err.txt"))));
			
			System.out.println("Critical Sections Analysis v.0.0.1");
			
			configureEnvironment();

			String srcFile = validateArgs(args);
			String srcFileName = FilenameUtils.getBaseName(srcFile);
			String basePath = FilenameUtils.getFullPath(srcFile);

			System.out.println("Verifying srcML exists...");
			boolean srcMLRecognized = checkIfExecutesSuccessfully(SRC_ML, "-h", false);
			if (!srcMLRecognized) {
				System.err.println("Unable to launch srcML");
				System.err.println("Please adjust the config file");
				System.err.println("Exiting parsing...");
				Runtime.getRuntime().exit(1);
			}

			System.out.println("Generating mapping *.xml file...");
			String srcXML = basePath + srcFileName + ".xml";
			boolean xmlGenerated = checkIfExecutesSuccessfully(SRC_ML, srcFile + " -o " + srcXML + " --position", true);
			if (!xmlGenerated) {
				System.err.println("Error when parsing source file: " + srcFile + " from srcML");
				Runtime.getRuntime().exit(2);
			}

			System.out.println("Parsing the *.xml file...");
			Document doc = getXMLDocument(srcXML);

			System.out.println("Beginning analysis...");
			Map<String, Function> entryPoints = getCodeEntryPoints(doc);

			System.out.println("==================");
			for (Function func : entryPoints.values()) {
				System.out.println(func);
				List<ExecutionPath> extractFunctionCallsInExecutionPaths = func.extractFunctionCallsInExecutionPaths();
				for (ExecutionPath path : extractFunctionCallsInExecutionPaths)
					System.out.println(path);
			}
			System.out.println("==================");
			System.out.println(Calendar.getInstance().getTime().toString() + ": " + srcFile);
			Collections.sort(logEntries);
			for (LogEntry entry : logEntries)
				System.out.println(entry.toString());
			System.out.println("==================");

			// new File(srcXML).delete();

			Runtime.getRuntime().exit(0);

		} catch (Exception ex) {
			handleException(ex);
		}
		finally {
			System.setErr(syserrBackUP);
			System.setOut(sysoutBackUP);
		}
	}

	private static Map<String, Function> getCodeEntryPoints(Document doc) {
		System.out.println("Extracting defined functions...");
		Map<String, Function> definedFunctions = extractDefinedFunctions(doc);

		System.out.println("Extracting functions' calls...");
		extractFunctionCalls(definedFunctions);

		Map<String, Function> entryPoints = new HashMap<String, Function>();
		for (Entry<String, Function> definedFunction : definedFunctions.entrySet())
			if (definedFunction.getValue().isEntryPoint())
				entryPoints.put(definedFunction.getKey(), definedFunction.getValue());

		if (entryPoints.size() > 0)
			return entryPoints;
		else {
			logEntries.add(
					new LogEntry(LogEntryType.warning, 0, 0, "", "no entry points found, all function are analyzed"));
			return definedFunctions;
		}
	}

	private static void extractFunctionCalls(Map<String, Function> definedFunctions) {
		for (Function function : definedFunctions.values()) {
			List<FunctionCall> functionCalls = function.extractFunctionCallsAndBlocks();
			for (FunctionCall call : functionCalls)
				if (call.getCalledFunction() == null)
					logEntries.add(new LogEntry(LogEntryType.warning, call.getLine(), call.getColumn(),
							call.getCalledFunctionName(), "undefined function being called"));
		}
	}

	private static Map<String, Function> extractDefinedFunctions(Document doc) {
		Map<String, Function> functions = new HashMap<String, Function>();
		Nodes functionsNodes = XPath.query("//function", doc);
		int functionsCount = functionsNodes.size();
		for (int i = 0; i < functionsCount; i++) {
			Element functionNode = (Element) functionsNodes.get(i);
			Element functionNameNode = (Element) XPath.query("./name", functionNode).get(0);
			String functionName = functionNameNode.getValue();
			Function function = functions.get(functionName);
			if (function == null) {
				function = new Function(functionName, functionNode, functions);
				functions.put(functionName, function);
			} else {
				int line = XPath.getLine(functionNameNode);
				int column = XPath.getColumn(functionNameNode);
				logEntries.add(
						new LogEntry(LogEntryType.warning, line, column, functionName, "repeated function definition"));
			}
		}
		return functions;
	}

	private static Document getXMLDocument(String documentFile)
			throws ValidityException, ParsingException, IOException {
		Builder xmlParser = new Builder();
		File file = new File(documentFile);
		Document doc = xmlParser.build(file);
		return doc;
	}

	private static String validateArgs(String[] args) {
		if (args.length < 1)
			displayNoSrcFileArgError();

		if (args[0].toLowerCase().equals("-h") || args[0].toLowerCase().equals("-help")) {
			System.out.println("The application takes 2 arguments: " + NEW_LINE
					+ " 1- Representing the source file to be parsed" + NEW_LINE
					+ " 2- An optional \"-expand\" indicating the user's desire to expand each function call's tree. In this case, only entry points are considerd");
			Runtime.getRuntime().exit(0);
		}

		String srcFile = "";
		for (String arg : args) {
			srcFile = arg;
			File file = new File(srcFile);
			if (!file.exists() || file.isDirectory()) {
				System.err.println("Please use a valid source file path");
				Runtime.getRuntime().exit(-1);
			}
		}
		if (srcFile.isEmpty())
			displayNoSrcFileArgError();
		return srcFile.replace("\\", "/");
	}

	private static void displayNoSrcFileArgError() {
		System.err.println("This application requires a source file as an argument. Aborting parsing...");
		Runtime.getRuntime().exit(-1);
	}

	private static void configureEnvironment() throws IOException {
		BufferedReader config = new BufferedReader(new FileReader("config"));
		try {
			System.out.println("Reading and setting paths for the executables...");
			String line = "";
			while ((line = config.readLine()) != null) {
				if (line.startsWith("#") || line.trim().isEmpty())
					continue;
				String[] nameAndValues = line.split("=");
				String name = nameAndValues[0].toLowerCase();
				if (name.equals("srcml"))
					SRC_ML = nameAndValues[1];
				else if (name.equals("cs")) {
					String[] matchers = nameAndValues[1].split("@");
					crticalSectionStartMatcher = matchers[0];
					crticalSectionEndMatcher = matchers[1];
				}
			}
		} catch (Exception ex) {
			handleException(ex);
		} finally {
			config.close();
		}

	}

	private static void handleException(Exception ex) {
		System.err.println(ex.toString());
		ex.printStackTrace(System.err);
		Runtime.getRuntime().exit(-1);
	}

	private static boolean checkIfExecutesSuccessfully(String cmd, String args, boolean showOutput)
			throws InterruptedException {
		if (executeCommand(cmd, args, showOutput) == 0)
			return true;
		return false;
	}

	private static int executeCommand(String cmd, String args, boolean showOutput) throws InterruptedException {
		try {
			Runtime rt = Runtime.getRuntime();
			String command = cmd.replace("\\", "/") + " " + args.replace("\\", "/");
			Process pr = rt.exec(command);

			consumeBuffer(pr.getInputStream(), showOutput, System.out);
			consumeBuffer(pr.getErrorStream(), showOutput, System.err);

			pr.waitFor();
			return pr.exitValue();
		} catch (IOException ex) {
			return -1;
		}
	}

	private static void consumeBuffer(InputStream buffer, boolean showOutput, PrintStream stream) throws IOException {
		BufferedReader in = new BufferedReader(new InputStreamReader(buffer));
		String line = null;
		while ((line = in.readLine()) != null)
			if (showOutput)
				stream.println(line);
	}
}
