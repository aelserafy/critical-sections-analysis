package model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import nu.xom.Element;
import nu.xom.Node;
import nu.xom.Nodes;
import utilities.XPath;

public class Block {
	public enum BlockType {
		FUNCTION, IF, THEN, ELSE, ELSE_IF, TERNARY, FOR, WHILE, DO
	};

	private final BlockType type;
	private final Element node;
	private final Block parent;
	private final int line;
	private final int column;
	private final Function rootFunction;
	private final Map<String, Function> visibleFunctions;
	private final Set<Element> accountedForFunctionCalls;
	private List<Block> children = new ArrayList<Block>();
	private List<FunctionCall> functionCalls = new ArrayList<FunctionCall>();

	public Block(BlockType type, Element node, Block parent, Function rootFunction) {
		this.type = type;
		this.node = node;
		this.parent = parent;
		this.rootFunction = rootFunction;
		this.line = XPath.getLine(node);
		this.column = XPath.getColumn(node);
		this.visibleFunctions = rootFunction.getVisibleFunctions();
		this.accountedForFunctionCalls = rootFunction.getAccountedForFunctionCalls();

		constructChildren(node);

		extractFunctionCalls();
	}

	private void extractFunctionCalls() {
		Nodes functionCallNodes = XPath.query(".//call", node);
		int functionCallCount = functionCallNodes.size();
		for (int j = 0; j < functionCallCount; j++) {
			Element callNode = (Element) functionCallNodes.get(j);
			if (accountedForFunctionCalls.contains(callNode))
				continue;
			else
				accountedForFunctionCalls.add(callNode);
			Element calledFunctionNameNode = (Element) XPath.query("./name", callNode).get(0);
			String calledFunctionName = calledFunctionNameNode.getValue();
			int line = XPath.getLine(calledFunctionNameNode);
			int column = XPath.getColumn(calledFunctionNameNode);
			Function calledFunctionDefinition = visibleFunctions.get(calledFunctionName);

			FunctionCall functionCall = new FunctionCall(line, column, calledFunctionName, calledFunctionDefinition,
					callNode);

			functionCalls.add(functionCall);
			rootFunction.addFunctionCall(functionCall);
		}
	}

	private boolean isLoop() {
		switch (type) {
		case DO:
		case FOR:
		case WHILE:
			return true;
		default:
			return false;
		}
	}

	public List<ExecutionPath> getFunctionCallsInAllPossibleExecutionPaths(List<ExecutionPath> executionPaths) {
		List<ExecutionPath> executionPathsCopy = new ArrayList<ExecutionPath>(),
				toBeReturned = new ArrayList<ExecutionPath>();
		for (ExecutionPath path : executionPaths) {
			ExecutionPath pathCopy = path.clone();
			pathCopy.getPassingBlocks().add(this);
			if (isLoop())
				pathCopy.setContainsLoop(true);
			executionPathsCopy.add(pathCopy);
			toBeReturned.add(pathCopy.clone());
		}
		for (Block child : children) {
			List<ExecutionPath> childPaths = child.getFunctionCallsInAllPossibleExecutionPaths(executionPathsCopy);
			toBeReturned.addAll(childPaths);
			if (isLoop() || type == BlockType.FUNCTION)
				executionPathsCopy.addAll(childPaths);
		}
		toBeReturned.removeAll(executionPaths);
		return toBeReturned;
	}

	private void constructChildren(Element node) {
		int childCount = node.getChildCount();
		for (int i = 0; i < childCount; i++) {
			Node child = node.getChild(i);
			if (!(child instanceof Element))
				continue;

			Element castedChild = (Element) child;
			String castedChildName = castedChild.getLocalName().toLowerCase();
			if (castedChildName.equals("if"))
				processBlock(BlockType.IF, castedChild);
			else if (castedChildName.equals("then"))
				processBlock(BlockType.THEN, castedChild);
			else if (castedChildName.equals("elseif"))
				processBlock(BlockType.ELSE_IF, castedChild);
			else if (castedChildName.equals("else"))
				processBlock(BlockType.ELSE, castedChild);
			else if (castedChildName.equals("ternary"))
				processBlock(BlockType.TERNARY, castedChild);
			else if (castedChildName.equals("for"))
				processBlock(BlockType.FOR, castedChild);
			else if (castedChildName.equals("while"))
				processBlock(BlockType.WHILE, castedChild);
			else if (castedChildName.equals("do"))
				processBlock(BlockType.DO, castedChild);
			else
				constructChildren(castedChild);
		}
	}

	private void processBlock(BlockType type, Element blockNode) {
		Block block = new Block(type, blockNode, this, rootFunction);
		children.add(block);
	}

	public BlockType getType() {
		return type;
	}

	public Block getParent() {
		return parent;
	}

	public List<FunctionCall> getFunctionCalls() {
		return functionCalls;
	}

	@Override
	public String toString() {
		String json = "";
		for (FunctionCall call : functionCalls)
			json += call.getCalledFunctionName() + " ";
		return json;
	}
}
