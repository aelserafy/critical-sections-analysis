package model;

import java.util.ArrayList;
import java.util.List;

public class ExecutionPath {
	private List<Block> passingBlocks = new ArrayList<Block>();
	private boolean containsLoop;

	public List<Block> getPassingBlocks() {
		return passingBlocks;
	}
	
	public List<FunctionCall> getFunctionsCalled() {
		List<FunctionCall> functionsCalled = new ArrayList<FunctionCall>();
		for(Block block:passingBlocks)
			functionsCalled.addAll(block.getFunctionCalls());
		return functionsCalled;
	}

	public ExecutionPath clone() {
		ExecutionPath clone = new ExecutionPath();
		clone.containsLoop = containsLoop;
		clone.passingBlocks.addAll(passingBlocks);
		return clone;
	}
	
	public void setContainsLoop(boolean containsLoop) {
		this.containsLoop = containsLoop;
	}

	@Override
	public String toString() {
		StringBuilder str = new StringBuilder();
		for (Block block:passingBlocks)
			str.append(block);
		return str.toString();
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof ExecutionPath))
			return false;
		ExecutionPath other = (ExecutionPath) obj;
		if (passingBlocks.size() != other.passingBlocks.size())
			return false;
		for (Block block : passingBlocks)
			if (!other.passingBlocks.contains(block))
				return false;
		return true;
	}
}
