package model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import nu.xom.Element;
import utilities.XPath;

public class Function {
	private final String name;
	private final Element definitionNode;
	private Block functionBlock;
	private final Map<String, Function> visibleFunctions;
	private final Set<Element> accountedForFunctionCalls = new HashSet<Element>();
	private List<FunctionCall> functionCalls = new ArrayList<FunctionCall>();
	private Set<Function> callingFunctions = new HashSet<Function>();
	private List<ExecutionPath> executionPaths = new ArrayList<ExecutionPath>();

	public Function(String name, Element definitionNode, Map<String, Function> definedFunctions) {
		this.name = name;
		this.definitionNode = definitionNode;
		this.visibleFunctions = definedFunctions;
	}

	public List<FunctionCall> extractFunctionCallsAndBlocks() {
		functionBlock = new Block(Block.BlockType.FUNCTION, (Element) XPath.query("./block", definitionNode).get(0),
				null, this);
		return functionCalls;
	}

	public List<ExecutionPath> extractFunctionCallsInExecutionPaths() {
		executionPaths.add(new ExecutionPath());
		executionPaths = functionBlock.getFunctionCallsInAllPossibleExecutionPaths(executionPaths);
		return executionPaths;
	}

	public void addFunctionCall(FunctionCall functionCall) {
		functionCalls.add(functionCall);
		Function calledFunction = functionCall.getCalledFunction();
		if (calledFunction != null)
			calledFunction.callingFunctions.add(this);
	}

	public boolean isEntryPoint() {
		return 0 == callingFunctions.size();
	}

	public String getName() {
		return name;
	}

	public Element getDefinitionNode() {
		return definitionNode;
	}

	public Block getFunctionBlock() {
		return functionBlock;
	}

	public Map<String, Function> getVisibleFunctions() {
		return visibleFunctions;
	}

	public List<FunctionCall> getFunctionCalls() {
		return functionCalls;
	}

	public Set<Element> getAccountedForFunctionCalls() {
		return accountedForFunctionCalls;
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Function))
			return false;
		Function otherFunction = (Function) obj;
		return name.equals(otherFunction.name) && definitionNode.equals(otherFunction.definitionNode);
	}

	@Override
	public String toString() {
		return name + "{" + functionBlock + "}";
	}
}
