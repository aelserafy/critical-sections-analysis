package model;

import nu.xom.Element;

public class FunctionCall implements Comparable<FunctionCall> {
	private final int line;
	private final int column;
	private final Function calledFunction;
	private final String calledFunctionName;
	private final Element callNode;

	public FunctionCall(int line, int column, String calledFunctionName, Function calledFunction, Element callNode) {
		this.line = line;
		this.column = column;
		this.calledFunctionName = calledFunctionName;
		this.calledFunction = calledFunction;
		this.callNode = callNode;
	}

	public int getLine() {
		return line;
	}

	public int getColumn() {
		return column;
	}

	public String getCalledFunctionName() {
		return calledFunctionName;
	}

	public Function getCalledFunction() {
		return calledFunction;
	}

	public Element getCallNode() {
		return callNode;
	}

	@Override
	public int compareTo(FunctionCall other) {
		int lineComparisonResult = new Integer(line).compareTo(other.line);
		if (lineComparisonResult == 0)
			return new Integer(column).compareTo(other.column);
		return lineComparisonResult;
	}
	
	@Override
	public String toString() {
		return "(" + line + ", " + column + ")\t" + calledFunctionName; 
	}
}
