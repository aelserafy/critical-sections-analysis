package model;
public class LogEntry implements Comparable<LogEntry>{
	public enum LogEntryType {
		warning, error
	}
	
	private LogEntryType type;
	private int line;
	private int column;
	private String problematicIdentifier;
	private String msg;
	
	public LogEntry(LogEntryType type, int line, int column, String problematicIdentifier, String msg) {
		this.type = type;
		this.line = line;
		this.column = column;
		this.problematicIdentifier = problematicIdentifier;
		this.msg = msg;
	}
	
	public int getLine() {
		return line;
	}
	
	public int getColumn() {
		return column;
	}
	
	public String getProblematicIdentifier() {
		return problematicIdentifier;
	}
	
	public String getMsg() {
		return msg;
	}
	
	public LogEntryType getType() {
		return type;
	}
	
	@Override
	public String toString() {
		return "#" + type.toString() + ": (" + line + ", " + column + ") \t" + problematicIdentifier + "; \t" + msg;
	}

	@Override
	public int compareTo(LogEntry arg0) {
		return new Integer(line).compareTo(arg0.line);
	}
}
