package utilities;

import nu.xom.Element;
import nu.xom.Node;
import nu.xom.Nodes;
import nu.xom.XPathContext;

public class XPath {
	public static final String Base_Namespace_URI = "http://www.srcML.org/srcML/";
	public static final String Position_Namespace_URI = Base_Namespace_URI + "position";

	public static Nodes query(String query, Node root) {
		XPathContext context = new XPathContext("x", Base_Namespace_URI + "src");
		return root.query(query.replaceAll("/(\\w+)", "/x:$1"), context);
	}

	public static int getColumn(Element node) {
		return getPositionAttribute(node, "column");
	}

	public static int getLine(Element node) {
		return getPositionAttribute(node, "line");
	}

	public static int getPositionAttribute(Element node, String attributeName) {
		try {
			return Integer.parseInt(node.getAttributeValue(attributeName, Position_Namespace_URI));
		} catch (Exception ex) {
			try {
				Element nameNode = (Element) query(".//name", node).get(0);
				return getPositionAttribute(nameNode, attributeName);
			} catch (Exception ex1) {
				return -1;
			}
		}
	}
}
